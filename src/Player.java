public class Player {
    private int position;
    private char name;
    private int gamesWonCount;
    public Player (char inName) {
        name = inName;
        position = getPosition();
        gamesWonCount = 0;
    }
    public char getName() {
        return name;
    }
    public void setPosition(int pos) {
        position = pos;
    }
    public int getPosition() {
        return position;
    }
    public String toString() {
        return Character.toString(name);
    }
    public void addWin() {
        gamesWonCount++;
    }
    public int getGamesWon () {
       return gamesWonCount;
    }

}
