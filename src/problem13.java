public class problem13 {
    private int x;
    private double y;
    public problem13() {
        x = 0;
        y = 0.0;
    }
    public problem13(int a, double b) {
        x = getValueX(a);
        y = getValueY(b);
    }

    public int getValueX (int i) {
        x = i;
       return x;
    }
    public double getValueY (double d) {
        y = d;
        return y;
    }
}