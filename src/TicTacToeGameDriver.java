import java.util.Scanner;
/**
 * Emelie Gage
 * Homework 1
 * Materials used: Class notes, textbook, Professor Harms
 * 1) set up class structure for Player
 * 2) set up structure for board
 * 3) add instructions in gamedriver
 * 4) test out any issues
 * Homework answers:
 * 1: C
 * 2: B
 * 3: A
 * 4: D
 * 5: A
 * 6: A
 * 7: A
 * 8: A
 * 9: C
 * 10: A
 * 11: B
 * 12: C
 * 13: see problem13
 */
public class TicTacToeGameDriver {
    public static void main(String[] args) {
        final int turns = 9;
        int round=0;
        boolean flag = true, good=true;
        char inProgress = 'I';
        GameBoard newGame;
        Player [] player = new Player[2];
        Player X = new Player('X');
        Player O = new Player('O');
        player[0]= X;
        player[1] = O;
        do {
            Player currentPlayer = X;
            Scanner keyboard = new Scanner(System.in);
            System.out.println("Let's play tic tac toe!");
            round = 0;
            inProgress = 'I';
            newGame = new GameBoard();
            while(round<turns&&inProgress=='I') {
                if (round % 2 == 0) {
                    currentPlayer = X;
                } else
                    currentPlayer = O;
                System.out.print(newGame.toString());
                System.out.println("Player " + currentPlayer.getName() + ": please choose a position to play!");
                int input = keyboard.nextInt();
                currentPlayer.setPosition(input);
                good = newGame.playerMove(input, currentPlayer);
                if(!good){
                    System.out.println("Invalid move! Please choose a free move on the board.");
                }
                else
                    round++;
                inProgress = newGame.checkForWinner();
            }
            if(inProgress=='I'&&round==turns)
                System.out.println("Tie, Cat game!");
            else {
                System.out.println(currentPlayer.getName() + " is the winner!");
                currentPlayer.addWin();
            }
            for(int i=0; i<2; i++) {
                System.out.println(player[i]+" has won "+ player[i].getGamesWon()+ " games.");
            }
            System.out.println("Do you want to play again? Y/N");
            keyboard.nextLine(); //this throws away the enter key entered after last number.
            String answer = keyboard.nextLine();
            if(answer.charAt(0)=='Y'||answer.charAt(0)=='y')
                flag = true;
            else
                flag = false;
        }
        while(flag);
        System.out.println("Thank you for playing!");
    }
}
