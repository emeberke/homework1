public class GameBoard {
    private char [] [] board;
    private char winner;
    public GameBoard() {
        board = new char [3] [3];
        int count = 1;
        for (int row = 0; row<board.length; row++) {
            for (int col = 0; col < board[row].length; col++) {
                board[row][col]=(char) ('0'+count);
                count++;
            }
        }
    }
    public boolean playerMove(int position, Player p) {
        int row = (position-1)/3, col = (position -1)%3;
        boolean flag = true;
        //convert values in board to int? then compare to input
        if(board[row][col]=='X'||board[row][col]=='O')
            flag = false;
        else if(position>9||position<1)
            flag = false;
        else
            board[row][col] = p.getName();
        return flag;
    }
    public char checkForWinner() {
        winner = 'I';
        for(int row = 0; row<board.length; row++) {
            if(board[row][0]==board[row][1]&&board[row][0]==board[row][2])
                winner=board[row][0];
        }
        for(int col = 0; col <board[0].length; col++) {
            if(board[0][col]==board[1][col]&&board[2][col]==board[0][col])
                winner = board[0][col];
        }
        if(board[0][0]==board[1][1]&&board[0][0]==board[2][2])
            winner = board[0][0];
        if(board[0][2]==board[1][1]&&board[2][0]==board[1][1])
            winner = board[0][2];
        return winner;
    }
    public String toString() {
        String output = "";
        for(int i=0; i<board.length; i++){
            for(int j=0; j<board[i].length; j++) {
                output += board[i][j]+" ";}
            output+="\n";
        }
        return output;
    }
}
/**
 this.extraCourses = new String[extraCourses.length];
 for(int i=0; i<extraCourses.length; i++)
 this.extraCourses[i] = extraCourses[i];*/